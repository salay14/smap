package eu.dusansalay.pricetagscanner.model.db

import com.dbflow5.annotation.Database

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
object AppDatabase {

    const val NAME: String = "AppDatabase"
    const val VERSION: Int = 1

}