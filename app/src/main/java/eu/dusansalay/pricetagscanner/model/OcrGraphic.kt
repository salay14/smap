package eu.dusansalay.pricetagscanner.model

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import com.google.android.gms.vision.text.TextBlock
import eu.dusansalay.pricetagscanner.ui.camera.GraphicOverlay

class OcrGraphic : GraphicOverlay.Graphic {

    private val mText: TextBlock

    private var sRectPaint: Paint? = null
    private var sTextPaint: Paint? = null

    private val TEXT_COLOR = Color.WHITE

    private var mId: Int
        get() {
            return this.mId
        }
        set(value) {
            this.mId = value
        }

    constructor(overlay: GraphicOverlay<*>?, mText: TextBlock
    ) : super(overlay) {

        this.mText = mText


        if (sRectPaint == null) {
            sRectPaint = Paint()
            sRectPaint!!.setColor(TEXT_COLOR)
            sRectPaint!!.setStyle(Paint.Style.STROKE)
            sRectPaint!!.setStrokeWidth(4.0f)
        }

        if (sTextPaint == null) {
            sTextPaint = Paint()
            sTextPaint!!.setColor(TEXT_COLOR)
            sTextPaint!!.setTextSize(54.0f)
        }

        postInvalidate()
    }


    fun getTextBlock(): TextBlock {
        return mText
    }


    /**
     * Draws the text block annotations for position, size, and raw value on the supplied canvas.
     */
    override fun draw(canvas: Canvas?) {

        val text = mText

        // Draws the bounding box around the TextBlock.
        val rect = RectF(text.boundingBox)
        rect.left = translateX(rect.left)
        rect.top = translateY(rect.top)
        rect.right = translateX(rect.right)
        rect.bottom = translateY(rect.bottom)
        canvas!!.drawRect(rect, sRectPaint)

        // Break the text into multiple lines and draw each one according to its own bounding box.
        val textComponents = text.components
        for (currentText in textComponents) {
            val left = translateX(currentText.boundingBox.left.toFloat())
            val bottom = translateY(currentText.boundingBox.bottom.toFloat())
            canvas.drawText(currentText.value, left, bottom, sTextPaint)
        }


    }


    /**
     * Checks whether a point is within the bounding box of this graphic.
     * The provided point should be relative to this graphic's containing overlay.
     * @param x An x parameter in the relative context of the canvas.
     * @param y A y parameter in the relative context of the canvas.
     * @return True if the provided point is contained within this graphic's bounding box.
     */
    override fun contains(x: Float, y: Float): Boolean {

        val text = mText ?: return false
        val rect = RectF(text.boundingBox)
        rect.left = translateX(rect.left)
        rect.top = translateY(rect.top)
        rect.right = translateX(rect.right)
        rect.bottom = translateY(rect.bottom)
        return rect.left < x && rect.right > x && rect.top < y && rect.bottom > y
    }
}