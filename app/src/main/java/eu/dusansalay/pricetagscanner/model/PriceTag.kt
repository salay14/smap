package eu.dusansalay.pricetagscanner.model

import java.io.Serializable

class PriceTag : Serializable {

    var idEan: Int = 0
    var name: PriceTagDimension? = null
    var price: PriceTagDimension? = null

    constructor(idEan: Int, name: PriceTagDimension?, price: PriceTagDimension?) {
        this.idEan = idEan
        this.name = name
        this.price = price
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PriceTag

        if (idEan != other.idEan) return false
        if (name != other.name) return false
        if (price != other.price) return false

        return true
    }

    override fun hashCode(): Int {
        var result = idEan
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "PriceTag(idEan=$idEan, name=$name, price=$price)"
    }


}