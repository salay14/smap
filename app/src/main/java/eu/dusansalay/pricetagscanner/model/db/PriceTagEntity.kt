package eu.dusansalay.pricetagscanner.model.db

import com.dbflow5.annotation.Column
import com.dbflow5.annotation.PrimaryKey
import com.dbflow5.annotation.Table

@Table(database = AppDatabase::class)
data class PriceTagEntity(@PrimaryKey var eanId: Int, @Column var name: String? = null, @Column var price: Double)