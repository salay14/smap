package eu.dusansalay.pricetagscanner.model

import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import eu.dusansalay.pricetagscanner.ui.camera.GraphicOverlay

/**
 * A very simple Processor which receives detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 */
class OcrDetectorProcessor(private val mGraphicOverlay: GraphicOverlay<OcrGraphic>) : Detector.Processor<TextBlock> {

    /**
     * Frees the resources associated with this detection processor.
     */
    override fun release() {
        mGraphicOverlay.clear()
    }



    override fun receiveDetections(detections: Detector.Detections<TextBlock>?) {
        mGraphicOverlay.clear()
        val items = detections!!.getDetectedItems()
        for (i in 0 until items.size()) {
            val item = items.valueAt(i)
            val graphic = OcrGraphic(mGraphicOverlay, item)
            mGraphicOverlay.add(graphic)
        }
    }


}