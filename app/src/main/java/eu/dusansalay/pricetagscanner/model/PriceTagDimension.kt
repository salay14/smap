package eu.dusansalay.pricetagscanner.model

class PriceTagDimension {

    var height: Int = 0
    var width: Int = 0
    var value: String = ""

    constructor(height: Int, width: Int, value: String) {
        this.height = height
        this.width = width
        this.value = value
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PriceTagDimension

        if (height != other.height) return false
        if (width != other.width) return false
        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        var result = height
        result = 31 * result + width
        result = 31 * result + value.hashCode()
        return result
    }

    override fun toString(): String {
        return "PriceTagDimension(height=$height, width=$width, value='$value')"
    }


}