package eu.dusansalay.pricetagscanner.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import eu.dusansalay.pricetagscanner.R

class CaptureActivity : AppCompatActivity() {

    private val TAG = "OcrCaptureActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ocr_capture)

    }
}