package eu.dusansalay.pricetagscanner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.common.api.CommonStatusCodes
import eu.dusansalay.pricetagscanner.activities.CaptureActivity
import eu.dusansalay.pricetagscanner.activities.OcrCaptureActivity

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    //switchs
    private var autoFocus: CompoundButton? = null
    private var useFlash: CompoundButton? = null

    private var statusMessage: TextView? = null
    private var textValue: TextView? = null

    private val RC_OCR_CAPTURE = 9003


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        statusMessage = findViewById(R.id.status_message) as TextView
        textValue = findViewById(R.id.text_value) as TextView

        autoFocus = findViewById(R.id.auto_focus) as CompoundButton
        useFlash = findViewById(R.id.use_flash) as CompoundButton


        val btn1_click = findViewById(R.id.button) as Button
        val intent2 = Intent(this, OcrCaptureActivity::class.java)
        intent2.putExtra(OcrCaptureActivity.AutoFocus, autoFocus!!.isChecked())
        intent2.putExtra(OcrCaptureActivity.UseFlash, useFlash!!.isChecked())

        btn1_click.setOnClickListener {
            Log.i(TAG, "Test");
            Toast.makeText(this, "Hello word", Toast.LENGTH_SHORT).show()
            startActivity(intent2)
        }

        startActivityForResult(intent2, RC_OCR_CAPTURE)


        val btn1_click2 = findViewById<Button>(R.id.button2)
        val intent = Intent(this, CaptureActivity::class.java)

        btn1_click2.setOnClickListener {
            startActivity(intent)
        }
    }


    /**
     * Called when an activity you launched exits, giving you the requestCode
     * you started it with, the resultCode it returned, and any additional
     * data from it.  The <var>resultCode</var> will be
     * {@link #RESULT_CANCELED} if the activity explicitly returned that,
     * didn't return any result, or crashed during its operation.
     * <p/>
     * <p>You will receive this call immediately before onResume() when your
     * activity is re-starting.
     * <p/>
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     * @see #startActivityForResult
     * @see #createPendingResult
     * @see #setResult(int)
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == RC_OCR_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val text = data.getStringExtra(OcrCaptureActivity.TextBlockObject)
                    statusMessage!!.setText(R.string.ocr_success)
                    textValue!!.setText(text)
                    Log.d(TAG, "Text read: $text")
                } else {
                    statusMessage!!.setText(R.string.ocr_failure)
                    Log.d(TAG, "No Text captured, intent data is null")
                }
            } else {
                statusMessage!!.setText(
                    String.format(
                        getString(R.string.ocr_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)
                    )
                )
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
