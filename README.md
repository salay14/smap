# SMAP

V dnešní době je velice zajímavé využít vlastnosti chytrých telefonů k rozpoznávání obrazových dat. 

V tomto projektu bych rád vytvořil aplikaci pro platformu android využívající obrazových dat z fotoaparátu.
Aplikace by uměla vyčíst data z nákupních štítků na jednotlivých cedulkách v obchodních řetězcích. Vyčtená data rozliší a strukturovaně uloží do tabulky.
Řešil by se tím problém dnešní doby, kde jednotlivé obchodní řetězce posílají své lidi s papírovým blokem a tužkou, kteří ručně opisují jednolivé ceny produktů.

Teoretickou část bych rád založil na následujícím článku. 
SMART koncept tohoto projektu vidím především v automatickém sbírání dat s využitím kamery. 

https://pdfs.semanticscholar.org/8a43/2184c311347a3851b0d3811d333662feb750.pdf 